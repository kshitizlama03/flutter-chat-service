library chat_service;

import 'package:chat_service/Core/Models/SingleSenderDetailModel.dart';

import 'BaseViewModel.dart';

class ChatViewModel extends BaseViewModel {
  ChatViewModel({required senderId, required url})
      : super(url: "{url}/{senderId}");

  @override
  otherFunction(response, listData) {
    response.data.map((item) =>
        listData.add(SingleSenderDetailModel.fromJson(item)).toList());
    return listData;
  }
}
