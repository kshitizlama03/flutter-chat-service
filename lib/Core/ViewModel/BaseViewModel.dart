library chat_service;

import 'package:chat_service/Services/ApiService/ApiService.dart';
import 'package:chat_service/Services/ApiService/Token.dart';
import 'package:flutter/cupertino.dart';

class BaseViewModel extends ChangeNotifier {
  late String _url;
  late String _jwtAuth;

  BaseViewModel({
    required String url,
  }) {
    _url = url;
    _jwtAuth = Token.jwtToken;
    getFromApi();
  }

  var response;
  List _data = [];

  List get getData => _data;

  getFromApi() async {
    ApiRequests request = new ApiRequests(jwtAuth: _jwtAuth);

    response = await request.getRequest(_url);
    // request.getRequest(_url);
    otherFunction(response, _data);

    notifyListeners();
  }

  otherFunction(response, listData) {}
}
