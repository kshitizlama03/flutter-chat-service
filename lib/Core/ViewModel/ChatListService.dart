library chat_service;

import 'package:chat_service/Core/Models/SingleSenderDetailModel.dart';

import 'BaseViewModel.dart';

class ChatListService extends BaseViewModel {
  ChatListService({required url}) : super(url: url);
  @override
  otherFunction(response, listData) {
    response.data.map((item) =>
        listData.add(SingleSenderDetailModel.fromJson(item)).toList());
  }
}
