import 'package:json_annotation/json_annotation.dart';

part 'SingleSenderDetailModel.g.dart';

@JsonSerializable()
class SingleSenderDetailModel {
  final String sendername;
  final String image;
  final int senderid;
  final String subject;
  final String detail;
  final String createdAt;
  final List? imageList;

  SingleSenderDetailModel({
    required this.sendername,
    required this.image,
    required this.senderid,
    required this.subject,
    required this.detail,
    required this.createdAt,
    required this.imageList,
  });

  factory SingleSenderDetailModel.fromJson(Map<String, dynamic> json) =>
      _$SingleSenderDetailModelFromJson(json);
  Map<String, dynamic> toJson() => _$SingleSenderDetailModelToJson(this);
}
