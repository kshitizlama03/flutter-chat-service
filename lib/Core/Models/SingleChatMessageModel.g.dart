// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SingleChatMessageModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SingleChatMessageModel _$SingleChatMessageModelFromJson(
    Map<String, dynamic> json) {
  return SingleChatMessageModel(
    isReceived: json['isReceived'] as bool,
    detail: json['detail'] as String,
    createdAt: json['createdAt'] as String,
  );
}

Map<String, dynamic> _$SingleChatMessageModelToJson(
        SingleChatMessageModel instance) =>
    <String, dynamic>{
      'isReceived': instance.isReceived,
      'detail': instance.detail,
      'createdAt': instance.createdAt,
    };
