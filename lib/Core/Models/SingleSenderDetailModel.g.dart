// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SingleSenderDetailModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SingleSenderDetailModel _$SingleSenderDetailModelFromJson(
    Map<String, dynamic> json) {
  return SingleSenderDetailModel(
    sendername: json['sendername'] as String,
    image: json['image'] as String,
    senderid: json['senderid'] as int,
    subject: json['subject'] as String,
    detail: json['detail'] as String,
    createdAt: json['createdAt'] as String,
    imageList: json['imageList'] as List<dynamic>?,
  );
}

Map<String, dynamic> _$SingleSenderDetailModelToJson(
        SingleSenderDetailModel instance) =>
    <String, dynamic>{
      'sendername': instance.sendername,
      'image': instance.image,
      'senderid': instance.senderid,
      'subject': instance.subject,
      'detail': instance.detail,
      'createdAt': instance.createdAt,
      'imageList': instance.imageList,
    };
