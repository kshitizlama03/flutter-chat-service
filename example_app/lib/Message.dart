import 'package:chat_service/Services/DateTimeService/DateTimeService.dart';
import 'package:flutter/material.dart';

import 'ChatScreen.dart';

class Message extends StatefulWidget {
  final String senderId;
  final String text;
  final String secondaryText;
  final String image;
  final String time;
  final bool isMessageRead;

  Message({
    required this.senderId,
    required this.text,
    required this.secondaryText,
    required this.image,
    required this.time,
    required this.isMessageRead,
  });

  @override
  _MessageState createState() => _MessageState();
}

class _MessageState extends State<Message> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: CircleAvatar(
          radius: 30.0,
          backgroundImage: NetworkImage(widget.image),
        ),
        title: Text(
          widget.text,
        ),
        subtitle: Text(
          widget.secondaryText,
        ),
        trailing: Text(
          DateTimeService.dateDifference(
            startDate: DateTime.parse(widget.time),
            endDate: DateTime.now(),
          ),
        ),
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return ChatScreen(senderId: widget.senderId);
          }));
        });
  }
}
