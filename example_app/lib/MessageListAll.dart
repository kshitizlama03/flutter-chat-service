import 'package:chat_service/Core/ViewModel/ChatListService.dart';
import 'package:flutter/material.dart';
import './Message.dart';
import 'package:provider/provider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MessageListAll extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: ChangeNotifierProvider<ChatListService>(
        create: (_) =>
            ChatListService(url: "https://trial.nivid.app/listMessages"),
        child: CustomScrollView(
          slivers: [
            SliverPadding(
              padding: EdgeInsets.only(top: 48, left: 24.0, right: 24.0),
              sliver: SliverToBoxAdapter(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Message',
                    ),
                    CircleAvatar(),
                  ],
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: searchMessage(),
            ),
            Consumer<ChatListService>(
              builder: (BuildContext context, ChatListService chatListService,
                      child) =>
                  SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
                  return Message(
                    senderId: chatListService.getData[index].sender.toString(),
                    text: chatListService.getData[index].sendername,
                    secondaryText: chatListService.getData[index].detail,
                    image: (chatListService.getData[index].imgarray != null)
                        ? chatListService.getData[index].imgarray[0]
                        : "https://www.worldfuturecouncil.org/wp-content/uploads/2020/02/dummy-profile-pic-300x300-1.png", // default image rakhdeko
                    time: chatListService.getData[index].created_at,
                    isMessageRead: (index == 0 || index == 3) ? true : false,
                  );
                }, childCount: chatListService.getData.length),
              ),
            ),
          ],
        ),
      ),
    );
  }

  searchMessage() {
    return Container(
      margin: EdgeInsets.all(20.0),
      height: 140.h,
      child: TextField(
        obscureText: true,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.search),
          hintText: 'Search Message',
          hintStyle: TextStyle(
              fontFamily: 'Poppins', color: Colors.grey, fontSize: 48.sp),
          fillColor: Colors.white,
          filled: true,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(color: Colors.white),
          ),
        ),
      ),
    );
  }
}
